package msgraph

import (
	"context"
	"io/ioutil"
	"os"

	"github.com/AzureAD/microsoft-authentication-library-for-go/apps/cache"
	"gitlab.com/andrewheberle/ubolt"
	kvstore "gitlab.com/andrewheberle/ubolt-kvstore/client"
)

type Accessor interface {
	cache.ExportReplace
	Close() error
}

// FileCache stores the access token cache in a flat file
type FileCache struct {
	file string
}

func NewFileCache(file string) (*FileCache, error) {
	return &FileCache{file}, nil
}

func (t *FileCache) Replace(cache cache.Unmarshaler, key string) {
	jsonFile, err := os.Open(t.file)
	if err != nil {
		return
	}
	defer jsonFile.Close()

	data, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		return
	}
	err = cache.Unmarshal(data)
	if err != nil {
		return
	}
}

func (t *FileCache) Export(cache cache.Marshaler, key string) {
	data, err := cache.Marshal()
	if err != nil {
		return
	}
	err = ioutil.WriteFile(t.file, data, 0600)
	if err != nil {
		return
	}
}

func (t *FileCache) Close() error {
	return nil
}

// UboltCache stores the access token cache in a github.com/etcd-io/bbolt based key-value store using the gitlab.com/andrewheberle/ubolt package
type UboltCache struct {
	db *ubolt.BDB
}

func NewUboltCache(file string) (*UboltCache, error) {
	db, err := ubolt.OpenBucket(file, []byte("cache"))
	if err != nil {
		return nil, err
	}

	return &UboltCache{db: db}, nil
}

func (t *UboltCache) Replace(cache cache.Unmarshaler, key string) {
	data, err := t.db.GetE([]byte(key))
	if err != nil {
		return
	}

	if err := cache.Unmarshal(data); err != nil {
		return
	}
}

func (t *UboltCache) Export(cache cache.Marshaler, key string) {
	// unmarshal data from cache
	data, err := cache.Marshal()
	if err != nil {
		return
	}

	// set key value from data
	if err := t.db.Put([]byte(key), data); err != nil {
		return
	}
}

func (t *UboltCache) Close() error {
	return t.db.Close()
}

type UboltKvStore struct {
	client *kvstore.KvStoreClient
}

func UboltKvStoreCache(address, cert string, insecure bool) (*UboltKvStore, error) {
	ctx := context.Background()

	client, err := kvstore.Connect(ctx, address, cert, insecure)
	if err != nil {
		return nil, err
	}

	return &UboltKvStore{client: client}, nil
}

func (t *UboltKvStore) Replace(cache cache.Unmarshaler, key string) {
	data, err := t.client.Get(context.Background(), "cache", key)
	if err != nil {
		return
	}

	if err := cache.Unmarshal(data); err != nil {
		return
	}
}

func (t *UboltKvStore) Export(cache cache.Marshaler, key string) {
	// unmarshal data from cache
	data, err := cache.Marshal()
	if err != nil {
		return
	}

	// set key value from data
	if err := t.client.Put(context.Background(), "cache", key, data); err != nil {
		return
	}
}

func (t *UboltKvStore) Close() error {
	return t.Close()
}
