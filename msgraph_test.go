package msgraph

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

type HTTPClientMock struct {
}

func (h HTTPClientMock) Do(r *http.Request) (*http.Response, error) {
	var page int
	var pages = []GraphPagedResponse{
		{
			NextLink: "/users/page2",
			Value: &[]json.RawMessage{
				[]byte("{\"user\": \"user1\"}"),
				[]byte("{\"user\": \"user2\"}"),
				[]byte("{\"user\": \"user3\"}"),
			},
		},
		{
			NextLink: "/users/page3",
			Value: &[]json.RawMessage{
				[]byte("{\"user\": \"user4\"}"),
				[]byte("{\"user\": \"user5\"}"),
				[]byte("{\"user\": \"user6\"}"),
			},
		},
		{
			Value: &[]json.RawMessage{
				[]byte("{\"user\": \"user7\"}"),
				[]byte("{\"user\": \"user8\"}"),
				[]byte("{\"user\": \"user9\"}"),
			},
		},
	}

	switch r.URL.Path {
	case "/users":
		page = 0
	case "/users/page2":
		page = 1
	case "/users/page3":
		page = 2
	case "/users/user1":
		resp := &http.Response{
			Status:     "200 OK",
			StatusCode: http.StatusOK,
			Body:       io.NopCloser(strings.NewReader("{\"user\":\"user1\"}")),
			Header:     http.Header{},
		}
		resp.Header.Add("Content-Type", "application/json")
		return resp, nil
	default:
		return &http.Response{
			Status:     "404 Not Found",
			StatusCode: http.StatusNotFound,
		}, nil
	}
	buf := new(bytes.Buffer)
	encoder := json.NewEncoder(buf)
	_ = encoder.Encode(pages[page])
	resp := &http.Response{
		Status:     "200 OK",
		StatusCode: http.StatusOK,
		Body:       io.NopCloser(buf),
		Header:     http.Header{},
	}
	resp.Header.Add("Content-Type", "application/json")
	return resp, nil
}

func Test_dorequest(t *testing.T) {
	var pagedwant json.RawMessage
	var want json.RawMessage

	b, _ := json.Marshal([]json.RawMessage{
		[]byte("{\"user\": \"user1\"}"),
		[]byte("{\"user\": \"user2\"}"),
		[]byte("{\"user\": \"user3\"}"),
		[]byte("{\"user\": \"user4\"}"),
		[]byte("{\"user\": \"user5\"}"),
		[]byte("{\"user\": \"user6\"}"),
		[]byte("{\"user\": \"user7\"}"),
		[]byte("{\"user\": \"user8\"}"),
		[]byte("{\"user\": \"user9\"}"),
	})
	_ = json.Unmarshal(b, &pagedwant)

	b, _ = json.Marshal(struct {
		User string `json:"user"`
	}{"user1"})
	_ = json.Unmarshal(b, &want)

	tests := []struct {
		name    string
		want    json.RawMessage
		client  HTTPClient
		uri     string
		wantErr bool
	}{
		{"paged request", pagedwant, HTTPClientMock{}, "/users", false},
		{"unpaged request", want, HTTPClientMock{}, "/users/user1", false},
	}

	for _, tt := range tests {
		got, err := dorequest(tt.client, tt.uri, http.MethodGet, "", nil)
		if tt.wantErr {
			assert.NotNil(t, err)
		} else {
			assert.Nil(t, err)
			assert.Equal(t, tt.want, got)
		}
	}
}

func Test_absendpoint(t *testing.T) {
	tests := []struct {
		name    string
		want    bool
		uri     string
		version string
	}{
		{"absolute", true, "https://graph.microsoft.com/v1.0/users", "v1.0"},
		{"relative", false, "/users", "v1.0"},
	}

	for _, tt := range tests {
		got := absendpoint(tt.uri, tt.version)
		assert.Equal(t, tt.want, got)
	}
}

func Test_generateuri(t *testing.T) {
	tests := []struct {
		name    string
		want    string
		uri     string
		version string
	}{
		{"absolute", "https://" + GraphApiHost + "/v1.0/users", "https://" + GraphApiHost + "/v1.0/users", "v1.0"},
		{"relative", "https://" + GraphApiHost + "/v1.0/users", "/users", "v1.0"},
		{"beta relative", "https://" + GraphApiHost + "/beta/users", "/users", "beta"},
		{"beta", "https://" + GraphApiHost + "/beta/users", "/users", "beta"},
	}

	for _, tt := range tests {
		got := generateuri(tt.uri, tt.version)
		assert.Equal(t, tt.want, got)
	}
}

type GraphMock struct {
}
