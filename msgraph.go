// Package msgraph implements some basic APIs to make working with the MS Graph API easier.
//
//
package msgraph

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"path"
	"strings"

	"github.com/AzureAD/microsoft-authentication-library-for-go/apps/confidential"
)

const GraphApiHost = "graph.microsoft.com"

type Graph struct {
	app     confidential.Client
	version string
	client  *http.Client
}

type GraphPagedResponse struct {
	NextLink string             `json:"@odata.nextLink"`
	Value    *[]json.RawMessage `json:"value"`
}

type GraphResponse json.RawMessage

type HTTPClient interface {
	Do(*http.Request) (*http.Response, error)
}

const (
	StatusOK int = iota
	StatusOKPaged
	StatusThrottled
	StatusResponseError
	StatusParseError
)

type Response struct {
	Paged       bool
	Throttled   bool
	IsJson      bool
	ContentType string
	Data        []byte
}

func (r *Response) ToJson() (json.RawMessage, error) {
	var jsonResponse json.RawMessage

	if !r.IsJson {
		return nil, fmt.Errorf("Not a JSON response")
	}

	if r.Throttled {
		return nil, fmt.Errorf("Response was throttled")
	}

	if r.Data == nil {
		return nil, fmt.Errorf("No response body")
	}

	if err := json.Unmarshal(r.Data, &jsonResponse); err != nil {
		return nil, err
	}

	return jsonResponse, nil
}

// RequestError defines an error type for errors that occur during a Graph API request
type RequestError struct {
	err error
}

func (err *RequestError) Error() string {
	return fmt.Sprintf("Request error: %s", err.err)
}

// ResponseError defines an error type for errors that occur during the handling of a Graph API response
type ResponseError struct {
	statusCode int
	err        error
}

func (err *ResponseError) Error() string {
	return fmt.Sprintf("Response error: %s", err.err)
}

var ErrorUnsupportedGraphVersion error = fmt.Errorf("usupported API version")

// NewGraph creates a new Graph application instance that can be used for subsequent access to the Graph API (v1.0)
//
// The clientid, tenantid and secret correspond to configuration of an App Registration in Azure AD
//
// The accessor defines a cache that can be used to persist access issued tokens
func NewGraph(clientid, tenantid, secret string, accessor Accessor) (*Graph, error) {
	return newgraphversion(clientid, tenantid, secret, "v1.0", accessor)
}

// NewBetaGraph creates a new Graph application instance that can be used for subsequent access to the Graph API (beta)
func NewBetaGraph(clientid, tenantid, secret string, accessor Accessor) (*Graph, error) {
	return newgraphversion(clientid, tenantid, secret, "beta", accessor)
}

func newgraphversion(clientid, tenantid, secret, version string, accessor Accessor) (*Graph, error) {
	if version != "beta" && version != "v1.0" {
		return nil, ErrorUnsupportedGraphVersion
	}

	// create credential from provided secret
	cred, err := confidential.NewCredFromSecret(secret)
	if err != nil {
		return nil, err
	}

	// build authority based on provided tenant id
	authority := fmt.Sprintf("https://login.microsoftonline.com/%s", tenantid)

	// create new application
	app, err := func() (confidential.Client, error) {
		if accessor != nil {
			return confidential.New(clientid, cred, confidential.WithAuthority(authority), confidential.WithAccessor(accessor))
		}

		return confidential.New(clientid, cred, confidential.WithAuthority(authority))
	}()
	if err != nil {
		return nil, err
	}

	return &Graph{app, version, &http.Client{}}, nil
}

// AcquireToken returns an confidential.AuthResult that can be used in requests to the MS Graph API
func (graph *Graph) AcquireToken() (confidential.AuthResult, error) {
	var scopes = []string{"https://" + GraphApiHost + "/.default"}

	// attempt to acquire token from cache or via a refresh token
	result, err := graph.app.AcquireTokenSilent(context.Background(), scopes)
	if err != nil {
		// retry using the provided credentials
		result, err = graph.app.AcquireTokenByCredential(context.Background(), scopes)
		if err != nil {
			return confidential.AuthResult{}, err
		}
	}

	return result, nil
}

func generateuri(endpoint, version string) string {
	if absendpoint(endpoint, version) {
		return endpoint
	}

	uri := fmt.Sprintf("https://%s", path.Join(GraphApiHost, version, endpoint))
	return uri
}

func absendpoint(endpoint, version string) bool {
	return strings.HasPrefix(endpoint, fmt.Sprintf("https://%s", path.Join(GraphApiHost, version)))
}

func (graph *Graph) DoRequest(uri string) (json.RawMessage, error) {
	// ensure endpoint is absolute (ie https://graph.microsoft.com/VERSION/...)
	if !absendpoint(uri, graph.version) {
		uri = generateuri(uri, graph.version)
	}

	// do some basic validation of supplied uri
	if _, err := url.Parse(uri); err != nil {
		return nil, err
	}

	// grab token
	result, err := graph.AcquireToken()
	if err != nil {
		return nil, err
	}

	// new http client
	client := &http.Client{}

	// perform request
	return dorequest(client, uri, result.AccessToken, http.MethodGet, nil)
}

// Get performs a HTTP GET request to the specified Graph API endpoint
func (graph *Graph) Get(uri string) (*Response, error) {
	return graph.request(http.MethodGet, uri, nil, false)
}

// Post performs a HTTP POST request to the specified Graph API endpoint
func (graph *Graph) Post(uri string, body io.ReadCloser) (*Response, error) {
	return graph.request(http.MethodPost, uri, body, false)
}

// Delete performs a HTTP DELETE request to the specified Graph API endpoint
func (graph *Graph) Delete(uri string) (*Response, error) {
	return graph.request(http.MethodDelete, uri, nil, false)
}

// Patch performs a HTTP PATCH request to the specified Graph API endpoint
func (graph *Graph) Patch(uri string, body io.ReadCloser) (*Response, error) {
	return graph.request(http.MethodPatch, uri, body, false)
}

// GetEventual performs a HTTP GET request to the specified Graph API endpoint
func (graph *Graph) GetEventual(uri string, eventualConsistency bool) (*Response, error) {
	return graph.request(http.MethodGet, uri, nil, eventualConsistency)
}

func (graph *Graph) request(method, uri string, body io.ReadCloser, eventualConsistency bool) (*Response, error) {
	// set up request
	req, err := graph.NewRequestEventual(method, uri, body, eventualConsistency)
	if err != nil {
		//		fmt.Fprintf(os.Stderr, "Error in \"(graph *Graph) request\" from \"graph.NewRequestEventual\": %s", err)
		return nil, err
	}

	// perform request
	resp, err := graph.Do(req)
	if err != nil {
		//		fmt.Fprintf(os.Stderr, "Error in \"(graph *Graph) request\" from \"graph.Do\": %s", err)
		return nil, err
	}

	// parse response
	return ParseResponse(resp)
}

// NewRequest builds a new http.Request used for subsequent Graph API requests
func (graph *Graph) NewRequest(method, uri string, body io.ReadCloser) (*http.Request, error) {
	return graph.newrequest(method, uri, body, false)
}

// NewRequestEventual builds a new http.Request used for subsequent Graph API requests
func (graph *Graph) NewRequestEventual(method, uri string, body io.ReadCloser, eventualConsistency bool) (*http.Request, error) {
	return graph.newrequest(method, uri, body, eventualConsistency)
}

func (graph *Graph) newrequest(method, uri string, body io.ReadCloser, eventualConsistency bool) (*http.Request, error) {
	// ensure endpoint is absolute (ie https://graph.microsoft.com/VERSION/...)
	if !absendpoint(uri, graph.version) {
		uri = generateuri(uri, graph.version)
	}

	// do some basic validation of supplied uri
	if _, err := url.Parse(uri); err != nil {
		//		fmt.Fprintf(os.Stderr, "Error in \"(graph *Graph) newrequest\" from \"url.Parse\": %s", err)
		return nil, &RequestError{err}
	}

	// grab token
	result, err := graph.AcquireToken()
	if err != nil {
		//		fmt.Fprintf(os.Stderr, "Error in \"(graph *Graph) newrequest\" from \"graph.AcquireToken\": %s", err)
		return nil, &RequestError{err}
	}

	// create request
	req, err := http.NewRequest(method, uri, body)
	if err != nil {
		//		fmt.Fprintf(os.Stderr, "Error in \"(graph *Graph) newrequest\" from \"http.NewRequest\": %s", err)
		return nil, &RequestError{err}
	}
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", result.AccessToken))

	// add ConsistencyLevel header if required
	if eventualConsistency {
		req.Header.Add("ConsistencyLevel", "eventual")
	}

	return req, nil
}

// Do performs a Graph API request
func (graph *Graph) Do(req *http.Request) (*http.Response, error) {
	resp, err := graph.client.Do(req)
	if err != nil {
		return nil, &RequestError{err}
	}

	return resp, nil
}

// ParseResponse performs parsing on the Graph API response
func ParseResponse(resp *http.Response) (*Response, error) {
	var jsonResponse json.RawMessage
	var paged GraphPagedResponse

	// check response wasn't throttled
	if resp.StatusCode == http.StatusTooManyRequests {
		return &Response{Throttled: true}, nil
	}

	// check response was 200 OK
	if resp.StatusCode != http.StatusOK {
		return nil, &ResponseError{resp.StatusCode, fmt.Errorf("Non-200 status code")}
	}

	// read reponse body
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, &ResponseError{resp.StatusCode, fmt.Errorf("Error reading response body: %w", err)}
	}

	// basic response
	response := &Response{
		ContentType: resp.Header.Get("Content-Type"),
		Data:        body,
	}

	// if response is not JSON just return as-is
	if !strings.HasPrefix(resp.Header.Get("Content-Type"), "application/json") {
		return response, nil
	}

	response.IsJson = true

	// decode to json.RawMessage
	if err := json.Unmarshal(response.Data, &jsonResponse); err != nil {
		return response, &ResponseError{resp.StatusCode, fmt.Errorf("Error during JSON unmarshal: %w", err)}
	}

	// attempt to unmarshal as paged response
	if err := json.Unmarshal(jsonResponse, &paged); err == nil {
		response.Paged = true
	}

	return response, nil
}

func dorequest(client HTTPClient, uri, method, token string, body io.Reader) (json.RawMessage, error) {
	var out, jsonResponse json.RawMessage
	var results []json.RawMessage

	for {
		var paged GraphPagedResponse

		// create request
		req, err := http.NewRequest(method, uri, body)
		if err != nil {
			return nil, err
		}
		req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", token))

		// perform request in its own function so body is closed each time
		if err := func() error {
			// do request
			resp, err := client.Do(req)
			if err != nil {
				return err
			}
			defer resp.Body.Close()

			// check response was 200 OK
			if resp.StatusCode != http.StatusOK {
				return fmt.Errorf("Bad response code: %d", resp.StatusCode)
			}

			// check response content type
			if !strings.HasPrefix(resp.Header.Get("Content-Type"), "application/json") {
				return fmt.Errorf("Bad content type: %s", resp.Header.Get("Content-Type"))
			}

			// decode to json.RawMessage
			decoder := json.NewDecoder(resp.Body)
			if err := decoder.Decode(&jsonResponse); err != nil {
				return err
			}

			return nil
		}(); err != nil {
			return nil, err
		}

		// attempt to unmarshal as paged response
		if err := json.Unmarshal(jsonResponse, &paged); err == nil {
			if paged.Value != nil {
				results = append(results, *paged.Value...)
			} else {
				results = append(results, jsonResponse)
				break
			}

			// paging is finished if nextlink is empty
			if paged.NextLink == "" {
				break
			}

			// set uri based on nextlink ready for next request
			uri = paged.NextLink
		} else {
			results = append(results, jsonResponse)
			break
		}
	}

	// with just one response only return that value
	if len(results) == 1 {
		return results[0], nil
	}

	// otherwise marshal []json.RawMessage then unmarshal back into json.RawMessage
	b, err := json.Marshal(results)
	if err != nil {
		return nil, err
	}
	if err := json.Unmarshal(b, &out); err != nil {
		return nil, err
	}

	return out, nil
}
