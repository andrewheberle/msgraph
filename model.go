package msgraph

import "time"

type ConditionalAccessPolicy struct {
	Name             string
	Conditions       ConditionalAccessConditionSet    `json:"conditions"`
	CreatedDateTime  time.Time                        `json:"createdDateTime"`
	DisplayName      string                           `json:"displayName"`
	GrantControls    ConditionalAccessGrantControls   `json:"grantControls"`
	ID               string                           `json:"id"`
	ModifiedDateTime time.Time                        `json:"modifiedDateTime"`
	SessionControls  ConditionalAccessSessionControls `json:"sessionControls"`
	State            string                           `json:"state"`
}

type ConditionalAccessConditionSet struct {
	Applications       ConditonalAccessApplications       `json:"applications"`
	Users              ConditonalAccessUsers              `json:"users,omitempty"`
	ClientApplications ConditonalAccessClientApplications `json:"clientApplications,omitempty"`
	ClientAppTypes     []string                           `json:"clientAppTypes,omitempty"`
	Devices            ConditonalAccessDevices            `json:"devices,omitempty"`
	Locations          ConditonalAccessLocations          `json:"locations,omitempty"`
	Platforms          ConditonalAccessPlatforms          `json:"platforms,omitempty"`
	SignInRiskLevels   []string                           `json:"signInRiskLevels,omitempty"`
	UserRiskLevels     []string                           `json:"userRiskLevels,omitempty"`
}

type ConditonalAccessApplications struct {
	IncludeApplications  []string `json:"includeApplications,omitempty"`
	ExcludedApplications []string `json:"excludedApplications,omitempty"`
	IncludeUserActions   []string `json:"includeUserActions,omitempty"`
}

type ConditonalAccessUsers struct {
	ExcludedGroups []string `json:"excludedGroups,omitempty"`
	ExcludedUsers  []string `json:"excludedUsers,omitempty"`
	ExcludedRoles  []string `json:"excludedRoles,omitempty"`
	IncludeGroups  []string `json:"includeGroups,omitempty"`
	IncludeUsers   []string `json:"includeUsers,omitempty"`
	IncludeRoles   []string `json:"includeRoles,omitempty"`
}

type ConditonalAccessClientApplications struct {
	ExcludedServicePrincipals []string `json:"excludedServicePrincipals,omitempty"`
	IncludeServicePrincipals  []string `json:"includeServicePrincipals,omitempty"`
}

type ConditonalAccessDevices struct {
	DeviceFilter ConditionalAccessFilter `json:"deviceFilter"`
}

type ConditionalAccessFilter struct {
	Mode string `json:"mode"`
	Rule string `json:"rule"`
}

type ConditonalAccessLocations struct {
	ExcludedLocations []string `json:"excludedLocations,omitempty"`
	IncludeLocations  []string `json:"includeLocations,omitempty"`
}

type ConditonalAccessPlatforms struct {
	ExcludedPlatforms []string `json:"excludedPlatforms,omitempty"`
	IncludePlatforms  []string `json:"includePlatforms,omitempty"`
}

type ConditionalAccessGrantControls struct {
	Operator                    string   `json:"operator"`
	BuiltInControls             []string `json:"builtInControls"`
	CustomAuthenticationFactors []string `json:"customAuthenticationFactors"`
	TermsOfUse                  []string `json:"termsOfUse"`
}

type ConditionalAccessSessionControls struct {
	ApplicationEnforcedRestrictions ApplicationEnforcedRestrictionsSessionControl `json:"applicationEnforcedRestrictions"`
	CloudAppSecurity                CloudAppSecuritySessionControl                `json:"cloudAppSecurity"`
	DisableResilienceDefaults       bool                                          `json:"disableResilienceDefaults"`
	PersistentBrowser               PersistentBrowserSessionControl               `json:"persistentBrowser"`
	SignInFrequency                 SignInFrequencySessionControl                 `json:"signInFrequency"`
}

type ApplicationEnforcedRestrictionsSessionControl struct {
	IsEnabled bool `json:"isEnabled"`
}

type CloudAppSecuritySessionControl struct {
	IsEnabled            bool   `json:"isEnabled"`
	CloudAppSecurityType string `json:"cloudAppSecurityType"`
}

type PersistentBrowserSessionControl struct {
	IsEnabled bool   `json:"isEnabled"`
	Mode      string `json:"mode"`
}

type SignInFrequencySessionControl struct {
	IsEnabled bool   `json:"isEnabled"`
	Type      string `json:"type"`
	Value     int32  `json:"value"`
}
